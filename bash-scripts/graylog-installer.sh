#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi
echo "Installing Pre-requisites..."
apt install wget apt-transport-https uuid-runtime pwgen -y && clear
echo "Installing Dependencies..."
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/oss-6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list
apt update && apt upgrade -y
apt install openjdk-8-jre-headless mongodb -y
apt install elasticsearch-oss -y && clear
echo "Configuring ElasticSearch..."
sed -i '/#cluster.name:/c\cluster.name: graylog' /etc/elasticsearch/elasticsearch.yml
sed -i "\$aaction.auto_create_index: false" /etc/elasticsearch/elasticsearch.yml
systemctl daemon-reload
systemctl enable elasticsearch.service
systemctl restart elasticsearch.service
clear
echo "Installing Graylog..."
wget https://packages.graylog2.org/repo/packages/graylog-3.0-repository_latest.deb
apt install ./graylog-3.0-repository_latest.deb -y && rm ./*.deb
apt update && apt install graylog-server && clear
echo "Configuring Graylog..."
ip=$(hostname  -I | cut -f1 -d' ')
sed -i "/#http_bind_address = 127.0.0.1:9000/c\http_bind_address = "$ip"" /etc/graylog/server/server.conf
echo -n "Enter desired admin password: "
read -s password
echo
root_pw_sha=$(echo -n $password | sha256sum | cut -d" " -f1)
sed -i "/^root_password_sha2/ s/$/ "$root_pw_sha"/" /etc/graylog/server/server.conf
pw=$(pwgen -N 1 -s 96)
printf "Your secret password is $pw\n"
sed -i "/^password_secret/ s/$/ "$pw"/" /etc/graylog/server/server.conf
systemctl start graylog-server
systemctl enable graylog-server
clear
printf "Graylog has been successfully installed and can be accessible from http://$ip:9000/\n"
printf "Your 'admin' password has been set to $password \nPlease keep it safe.\n"